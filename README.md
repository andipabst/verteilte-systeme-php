# PHP Vorlesung Kleebauer

### Konzept

Eine REST API (REpresentational State Transfer Accessible Programming Interface) ist eine Schnittstelle, um mit einem Webserver zu komunizieren.

Dabei werden verschiedene Aktionen aufgrund von HTTP Methoden (HTTP Verbs) (GET, POST, PUT, DELETE, etc.) durchgeführt.

Eine Aktion setzt sich somit aus zwei Komponenten zusammen: HTTP Methode und aufgerufene Datei, also z.B.

| Methode  | Datei        | Aktion 
|----------|--------------|--------
| GET      | user.php     | Gibt alle Nutzer zurück
| POST     | user.php     | Erstellt einen Nutzer
| GET      | meeting.php  | Gibt alle Meetings zurück
| POST     | meeting.php  | Erstellt ein Meeting


Die Dateien sind einfach die PHP Dateien. In einer Datei muss also nach den HTTP Methoden unterschieden werden. 
Diese HTTP Metode kann man mit
    $_SERVER['REQUEST_METHOD']
erhalten. Hier wird ein String zurückgeliefert, der den Namen der entsprechenden Methode enthält.

Anschließend kann man mittels swicth/case (oder auch mit if/else möglich) entsprechend unterscheiden, was man tun möchte:

GET -> Daten zurückgeben oder POST -> Daten eintragen
 
Bei einem GET ist hier Ende. Bei einem POST haben wir die Daten allerdings nur in einer Datenbank. Die Anforderung ist jedoch, dass die Daten in mehreren Datenbanken liegen.
Damit wir nicht direkten Zugriff auf die Datenbank brauchen und da der andere auch eine REST API hat, rufen wir einfach diese auf und teilen so die Daten mit.

Hier taucht aber ein Problem auf: Wenn diese REST API nicht erreichbar ist, führt das zu Dateninkonsistenzen.

Lösung: Wenn der POST an die andere REST API nicht erfolgreich war, werden die Daten in eine Log-Tabelle in der Datenbank geschrieben.
Wenn die andere Datenbank wieder online ist, frägt sie nach, ob es in der Zwischenzeit Änderungen gab. Falls ja, werden diese eingetragen.

### Aufbau

Beide Partner brauchen also die gleiche REST API und die gleiche Datenbank (siehe Zusatz)

Beide Partner müssen die IP-Adresse des jeweils anderen kennen und eintragen.

Es gibt drei Dateien: 
- config.php: Hier werden Einstellungen getätigt
- user.php: Hier werden Nutzer angelegt und ausgegeben
- logs.php: Diese Datei muss aufgerufen werden, wenn der Server offline war, um die Daten zu synchronisieren.

Problem: Wenn die Server immer gegenseitig POST user.php aufrufen, werden endlos Daten eingetragen -> POST Parameter 'fromapi', der true ist, wenn der wert nur eingetragen werden soll. 
Fehlt er, oder ist er false, werden die Daten an den anderen weitergegeben.

Datenbanktabellen:
- user(id, name, vorname, email)
- mssing(id, name, vorname, email)

### Zusatz

Die REST API und die Datenbank müssen nicht zwingend genau gleich sein. Es reicht, wenn sie die gleiche Funktionalität haben und wenn sie die gleichen HTTP Methoden und Dateinamen (Pfade) verwenden.
Die Implementierung, die dahinter liegt ist jedoch vollkommen egal, es kann also eine REST API, die in PHP geschrieben ist auch mit einer kommunizieren, die in Node.js oder Ruby on Rails geschrieben wurde.
Das ist ein Vorteil einer (REST) API.