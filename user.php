<?php

// Die Konfiguration einbinden
require_once('config.php');

//Verbindung zur Datenbank herstellen
$conn = new PDO('mysql:host=' . $DB_HOST . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);

// HTTP Methoden unterscheiden
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        // Führt die Abfrage durch
        $query = $conn->query("SELECT * FROM user");

        // gibt ein Array mit den Ergebnissen zurück
        $result = $query->fetchAll();

        // gibt die ergebnisse aus
        print_r($result);
        break;
    case 'POST':
        // POST Parameter erhalten
        $param_name = $_POST['name'];
        $param_vorname = $_POST['vorname'];
        $param_email = $_POST['email'];

        // wahr, falls es ein aufruf von der anderen api ist, und der inhalt nur eingetragen und nicht weitergegeben werden soll
        $param_fromapi = $_POST['fromapi'];

        // Fehlermeldung, wenn diese Parameter fehlen
        if (!isset($param_name) || !isset($param_vorname) || !isset($param_email)) {
            print("ERROR: WRONG PARAMS");
            return;
        }

        // SQL Statement vorbereiten (in die Stellen mit Doppelpunkt werden später die Werte eingefügt)
        $statement = $conn->prepare("INSERT INTO user (ID, name, vorname, email) VALUES (NULL, :name, :vorname, :email)");

        // binden der Parameter von oben an das statement (die werte werden also eingefügt)
        $statement->bindParam(':name', $param_name);
        $statement->bindParam(':vorname', $param_vorname);
        $statement->bindParam(':email', $param_email);

        // DIE SQL Abfrage durchführen
        $result = $statement->execute();

        // Fehler oder Erfolgsmeldung ausgeben
        if ($result == false) {
            print("ERROR: SQL INSERT FAILED");
        } else {
            print("SUCCESS");
        }

        // Daten an den Partner weitergeben, wenn sie nicht von ihm kommen
        if (!isset($param_fromapi) || $param_fromapi == false) {
            // Parameters für POST an Partner
            $data = array(
                'name' => $param_name,
                'vorname' => $param_vorname,
                'email' => $param_email,
                'fromapi' => true
            );

            // http query vorbereiten
            $query = http_build_query($data);

            // context für http call an Partner erstellen
            $context = stream_context_create(array(
                'http' => array(
                    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method' => 'POST',
                    'content' => $query,
                ),
            ));

            // send request and collect data
            $response = file_get_contents(
                $target = $URL_PARTNER,
                $use_include_path = false,
                $context);

            //Überprüfen, ob der Request beim anderen erfolgreich war und wenn nicht, die Daten in der Datenbank speichern
            if (!(strpos($response, 'SUCCESS') >= 0) || $response === FALSE) {
                // SQL Statement vorbereiten
                $log_statement = $conn->prepare("INSERT INTO missing (ID, name, vorname, email) VALUES (NULL, :name, :vorname, :email)");

                // binden der Parameter von oben an das statement (die werte werden also eingefügt)
                $log_statement->bindParam(':name', $param_name);
                $log_statement->bindParam(':vorname', $param_vorname);
                $log_statement->bindParam(':email', $param_email);

                //SQL statement ausführen
                $log_statement->execute();
            }
        }
        break;
}
?>

