<?php

// Die Konfiguration einbinden
require_once('config.php');

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    //Verbindung zur Datenbank herstellen
    $conn = new PDO('mysql:host=' . $DB_HOST . ';dbname=' . $DB_NAME, $DB_USER, $DB_PASSWORD);

    // Führt die Abfrage durch
    $query = $conn->query("SELECT * FROM missing");

    // gibt ein Array mit den Ergebnissen zurück
    $result = $query->fetchAll();

    // über alle einträge iterieren
    foreach ($result as $entry) {

        // Parameters für POST an Partner
        $data = array(
            'vorname' => $entry['vorname'],
            'name' => $entry['name'],
            'email' => $entry['email'],
            'fromapi' => true
        );

        // http query vorbereiten
        $query = http_build_query($data);

        // context für http call an Partner erstellen
        $context = stream_context_create(array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => $query,
            ),
        ));

        // send request and collect data
        $response = file_get_contents(
            $target = $URL_PARTNER,
            $use_include_path = false,
            $context);

        if ($response === FALSE) {
            //falls request fehlschlägt
        } else {
            if (strpos($response, 'SUCCESS') >= 0) {
                echo("empty it");
                // SQL Statement vorbereiten
                $log_statement = $conn->prepare("DELETE FROM missing WHERE vorname = :vorname AND name = :name AND email = :email");

                // binden der Parameter von oben an das statement (die werte werden also eingefügt)
                $log_statement->bindParam(':name', $entry['name']);
                $log_statement->bindParam(':vorname', $entry['vorname']);
                $log_statement->bindParam(':email', $entry['email']);

                // SQL Query ausführen
                echo($log_statement->execute());
            }
        }

        var_dump($response);

    }

    $conn = null;
}
?>